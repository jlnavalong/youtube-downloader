# youtube downloader
Antes de nada decir que esta guía esta hecha con las siguientes herramientas y versiones:

-Ubuntu 18.04

-De mysql estoy usando la versión 5.7.30

-De phpmyadmin la versión 4.6.6deb5

-De apache la versión 2.4.29

-De php la versión 7.2

-De node.js la versión 8.10.0

-De atom la versión 1.47.0


A continuación voy a describir los pasos que hay que realizar para instalar correctamente este proyecto en el sistema:

1-Instalar todas las herramientas en ubuntu:

-Atom se puede descargar desde la aplicación software de ubuntu

-Node.js ya esta incluido en el repositorio

-Ahora hay que ejecutar este comando para instalar apache, php, mysql y phpmyadmin

sudo apt install mysql-server apache2 phpmyadmin php7.2 php7.2-cli php7.2-common php7.2-mysql libapache2-mod-php7.2

2-configurar la instalación de mysql. Dejo enlace a la guía:

https://www.digitalocean.com/community/tutorials/como-instalar-mysql-en-ubuntu-18-04-es

3-Para acceder a phpmyadmin, abrir el navegador y escribir localhost/phpmyadmin, acceder como root, crear usuario "pepe" con contraseña "1", 
hay que asignarle todos los permisos y marcar la opción de crear base de datos con el mismo nombre

4-Crear dentro de la base de datos pepe una tabla con nombre "usuarios" y crear 5 campos:

"usuario", "contraseña" "nombre" "apellidos" tipo varchar con longitud de 100 caracteres y 1 campo mas como "id" tipo INT sin longitud 
y autoincremental

5-Solo faltaría dirigirse a la ruta /var/www/html/ mediante comandos, crear una carpeta con cualquier nombre 
como "proyecto" y dentro de esa carpeta pegar toda mi estructura de archivos

6-Moverse a la carpeta Server y escribir "node index.js"

7-Abrir el navegador que tengamos y escribir localhost/proyecto/index.php
